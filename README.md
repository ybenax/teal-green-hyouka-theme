# Teal-Green Hyouka Theme for Gnome

![](Flexing/1.png)

---

A Gnome theme inspired by [this beautiful piece of art](wallpaper.jpg). I tried to reverse-search the image, but I didn't manage to find the original artist.

**If you know the source, please let me know to credit them properly.**

* Literally all the theming of this setup relies on *Gnome Extensions.* I listed them in [extension-list](extension-list).

* I exported the settings for the extensions that allowed it; for the rest, I screenshoted the settings. If there's a smarter way to handle those, feel free to let me know.

* Place the corresponding theme folders within .icons and .themes at your home directory.

* Make sure you setup *Aylur's Widgets* before *Dash to Panel*. If you see some glitching on your top panel, turn the latter off and on.

**Note: so far, I have only tested this setup on *Pop!_OS 22.04 LTS*.**

## Screenshots

![](Flexing/2.png)

![](Flexing/3.png)

![](Flexing/4.png)

![](Flexing/5.png)

## Q&A

**"How do you get that cool borderless terminal on pic 4?"**

I use the kitty terminal. You can get it with `sudo apt install kitty`. The Pop Shell has an [extension setting](https://i.imgur.com/y0mXI6T.png) that allows you to hide window titles in Gnome. I'm also using some padding within the terminal to make it look a little less cluttered— just add `window_padding_width 20` to .config/kitty/kitty.conf with some text editor.

For the rounded corners on all windows, I use [this extension](https://extensions.gnome.org/extension/5237/rounded-window-corners/).

**"How do you make your taskbar and dock transparent (and floating) like in the 5th picture? Love the look."**

Thank you! If you only want to make your existing top bar transparent and floating, I recommend you use [this extension for transparency](https://extensions.gnome.org/extension/3960/transparent-top-bar-adjustable-transparency/). Then, to make it "floating", I use [this shell theme](https://drasite.com/flat-remix-gnome)— scroll down a little to find installation instructions, but bear in mind it may not be compatible with the COSMIC shell; if you see any breakage, you may want to [deactivate it from your Extensions app](https://i.imgur.com/EHjuhI4.png) to have your vanilla Gnome Shell instead. I guess what the theme does is make part of the bar invisible with some CSS trickery so it appears to be floating, so you probably could do the same by editing some files without having to install the whole theme; I'm no programmer so I can't really help there, but it *should* be possible.

On the other hand, if you want your bar to look and behave exactly like mine, you'd have to install the [Dash to Panel extension](https://extensions.gnome.org/extension/1160/dash-to-panel/). You'll have to use the settings of the extension to move the panel back to the top and shrink it to make it look like the vanilla top bar, but you get these [dynamic opacity settings](https://i.imgur.com/FOZ41Zh.png) in return. I set them up so the top bar goes to 100% opacity (a.k.a. no transparency at all) when there are any windows on-screen, and goes back to 20% opacity when there are no windows near the bar.

For the dock, I use [Dash to Dock](https://extensions.gnome.org/extension/307/dash-to-dock/). Same as before: you get opacity settings with the extension, but you'd have to [deactivate the COSMIC Dock](https://i.imgur.com/ZqwxLBH.png) that comes with Pop\_OS if you don't want them stacked on top of each other. If you're using the Dash to Panel extension I mentioned before too, you have to go to the Dash to Panel settings and [activate "Keep original gnome-shell dash"](https://i.imgur.com/ZFKGMZw.png) so the extension doesn't nuke your docks.

Hope it helps. Hugs!

**"What themes and icons are you using?"**

The Shell theme is the [Flat Remix theme by Daniruiz](https://drasite.com/flat-remix-gnome). The icon theme is [Candy Icons by EliverLara](https://github.com/EliverLara/candy-icons) (use the "filled" variant; it comes bundled in the same download). If you want to set it up exactly like mine, your Tweaks application setting should look [like this](https://i.imgur.com/ICXZPFM.png). [Here's the GTK](https://drasite.com/flat-remix-gtk).

**"So cool! Which WM are you using? I saw that you disabled Pop Shell, so it isn't Pop Shell TWM"**

[I didn't!](https://i.imgur.com/gAQzwE5.png) I just hid the tiling button on the top bar. I disabled most of the COSMIC extensions, but the Pop Shell is still there shining for me—it's actually one of the main reasons I chose Pop again after transitioning back from Arch, alongside what I consider some sane defaults, like full LUKS2 disk encryption and a dedicated recovery partition.

I'm experimenting with running Gnome with AwesomeWM, Qtile, or i3 in the same session, but honestly I'm not in a hurry— the Pop Shell is great.

**"Looks amazing, How do u make the dock indicators use the app icon color ?"**

Thank you! That's a [Dash to Dock](https://extensions.gnome.org/extension/307/dash-to-dock/) feature. I explain a little more about how you can set it up [here](https://www.reddit.com/r/pop_os/comments/13nldod/comment/jl07qq1/?utm_source=share&utm_medium=web2x&context=3). Then, go to your extension settings using the Extensions app (if you don't have it, you can install it with `sudo apt install gnome-shell-extensions`) and, under *"Appearance" > "Customize windows counter indicators" > Cog*, [enable "Use dominant color."](https://i.imgur.com/uqhb9Ed.png)

**"That's really awesome. I see you did a thing I've been trying to sort out for a bit now. How did you change the background behind the work spaces... that 'meta background'?"**

Thank you very much! You can try [this extension](https://extensions.gnome.org/extension/5856/overview-background/); I haven't tried it myself, but it seems like you should be able to change the overview background to whatever you want with it. Correct me if you try it and I'm utterly wrong.

In my case, I'm using the [Blur My Shell extension](https://extensions.gnome.org/extension/3193/blur-my-shell/), so it basically takes whatever background image I have and places it in the overview with a blur effect (though you can also remove the blur completely and just use the extension to set your background as your overview background). By default it will blur a lot of things around your desktop, like the top bar, for instance— just disable anything you don't want; I personally only leave the Overview blur active.

**"this is so good! may I ask what theme are you using? and also the workspace! thanks mate!"**

Thank you! I'm using the [Flat Remix theme by Daniruiz](https://drasite.com/flat-remix-gnome)! As for the workspace, I'm not entirely sure what it is you are asking for specifically, but I turned off most of Pop_OS's COSMIC extensions in the Extensions app (if you don't have it, you can get it with sudo apt install gnome-shell-extensions) to get my desktop back to vanilla Gnome. COSMIC is really cool, but it's not compatible with a lot of [Gnome extensions](https://extensions.gnome.org/) you need to customize your desktop further.

**"How did u get horizontal workspaces? And does it work with gestures?"**

If you [disable Pop COSMIC](https://i.imgur.com/lseiPmH.png) from your extensions, you're gonna get back to vanilla Gnome, which uses horizontal workspaces by default. Now, keep in mind that's gonna break your keybindings for workspace switching, at least the relative ones that is (switching to the next or previous workspace relative to the one you're currently in); that's because the keybindings to switch to the right and left workspace (a.k.a. horizontally) are hidden by default on Pop_OS. To unhide them, you need to edit 50-mutter-navigation.xml with any text editor of your preference:

`sudo gedit /usr/share/gnome-control-center/keybindings/50-mutter-navigation.xml`

or—

`sudo vim /usr/share/gnome-control-center/keybindings/50-mutter-navigation.xml`

or—

`sudo emacs /usr/share/gnome-control-center/keybindings/50-mutter-navigation.xml`

or (as a last resource, only if you're desperate)—

`sudo nano /usr/share/gnome-control-center/keybindings/50-mutter-navigation.xml`

Then, you change the values from true to false [here](https://i.imgur.com/LTxmF7h.png) and [here](https://i.imgur.com/fSHb2QS.png). That should bring back those keybindings to your [Gnome settings](https://i.imgur.com/fmq8Nso.png) app, so you can set them to whatever you see fit. You can actually get a lot of use out of that XML file I mentioned before— for instance, I added new entries for keybindings to [jump directly to workspaces 5 and 6](https://i.imgur.com/JXjSJGr.png) by simply [following the same syntax as the rest of the file](https://i.imgur.com/TQXOALa.png), but that's entirely optional.

Regarding your question about gestures, I genuinely don't use them, but I've seen [some extensions that seem to work for that](https://extensions.gnome.org/extension/4033/x11-gestures/). You'd have to try them yourself.

---

*Hugs!*
